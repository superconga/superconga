#!/usr/bin/env python3
# Copyright (C) 2019 and after, SuperConga team.
# All rights reserved.
# This file is part of the SuperConga Project, under the GNU LGPL v3
# license or higher. See LICENSE.txt for license information.

"""
Default settings for the postprocess plotter.
"""

# Default settings.
settings = {
    "limit": -1,
    "margin": 0.1,
    "subplot_width": 7.0,
    "subplot_height": 6.5,
    "fontsize": 14,
    "alphabet": True,
    "show_path": True,
    "use_tex": True,
    "colormap": "YlGnBu",
    "exterior_color": "dimgray",
    "dos_color": "black",
    "ldos_color": "red",
    "marker_color": "red",
}
