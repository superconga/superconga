//===------------------ dispersion.h - The dispersion class --------------===//
//
// Part of the SuperConga Project, under the GNU LGPL v3 license or higher.
// See LICENSE.txt for license information.
//
//===----------------------------------------------------------------------===//
///
/// \file
/// It contains the dispersion base class, and the classes derived from it, e.g.
/// a tight-binding dispersion on a square lattice.
///
//===----------------------------------------------------------------------===//
#ifndef CONGA_DISPERSION_TIGHT_BINDING_H_
#define CONGA_DISPERSION_TIGHT_BINDING_H_

#include "Dispersion.h"

#include <cmath>
#include <iostream>

namespace conga {
/// \brief Tight-binding dispersion class.
template <typename T> class DispersionTightBinding : Dispersion<T> {
private:
  // Nearest neighbour hopping. It is always 1, all other hopping terms, and the
  // chemical potential, is expressed in terms of it. It is our energy scale.
  const T m_hoppingNN = static_cast<T>(1.0);

  // Next-nearest neighbour.
  const T m_hoppingNNN;

  // Next-next-nearest neighbour.
  const T m_hoppingNNNN;

  // Chemical potential (or rather, Fermi level).
  const T m_chemicalPotential;

  // Anisotropy between x- and y-directions.
  // See e.g. Supplemental Material p.6 in:
  // E. Wahlberg et al., Science 373, 1506 (2021),
  // https://www.science.org/doi/abs/10.1126/science.abc8372
  const T m_hoppingAnisotropy = static_cast<T>(0.0);

public:
  /// \brief Constructor.
  ///
  /// \param inHoppingNNN - The next-nearest neighbour hopping.
  /// \param inHoppingNNNN - The next-next-nearest neighbour hopping.
  /// \param inChemicalPotential - The chemical potential.
  DispersionTightBinding(const T inHoppingNNN, const T inHoppingNNNN,
                         const T inChemicalPotential,
                         const T inHoppingAnisotropy = static_cast<T>(0.0))
      : Dispersion<T>(), m_hoppingNN(static_cast<T>(1.0)),
        m_hoppingNNN(inHoppingNNN), m_hoppingNNNN(inHoppingNNNN),
        m_chemicalPotential(inChemicalPotential),
        m_hoppingAnisotropy(inHoppingAnisotropy) {}

  /// \brief Evaluate the dispersion.
  ///
  /// \param inMomentum - The Fermi momentum (kx, ky).
  ///
  /// \return - The dispersion value.
  T evaluate(const vector2d<T> &inMomentum) const override {
    return evaluate(inMomentum[0], inMomentum[1]);
  }

  /// \brief Evaluate the dispersion.
  ///
  /// \param inMomentumX - The Fermi momentum along the x-axis.
  /// \param inMomentumY - The Fermi momentum along the y-axis.
  ///
  /// \return - The dispersion value.
  T evaluate(const T inMomentumX, const T inMomentumY) const override {
    // Hopping anisotropy.
    const T anisotropyPlus = static_cast<T>(1.0) + m_hoppingAnisotropy;
    const T anisotropyMinus = static_cast<T>(1.0) - m_hoppingAnisotropy;

    // Nearest neighbour.
    const T factorNN = -static_cast<T>(2.0) * m_hoppingNN;
    const T termNN = factorNN * (anisotropyPlus * std::cos(inMomentumX) +
                                 anisotropyMinus * std::cos(inMomentumY));

    // Next-nearest neighbour.
    const T factorNNN = -static_cast<T>(4.0) * m_hoppingNNN;
    const T termNNN = factorNNN * std::cos(inMomentumX) * std::cos(inMomentumY);

    // Next-next-nearest neighbour.
    const T factorNNNN = -static_cast<T>(2.0) * m_hoppingNNNN;
    const T termNNNN =
        factorNNNN *
        (anisotropyPlus * std::cos(static_cast<T>(2.0) * inMomentumX) +
         anisotropyMinus * std::cos(static_cast<T>(2.0) * inMomentumY));

    return (termNN + termNNN + termNNNN - m_chemicalPotential);
  }

  /// \brief Analytical derivative of the dispersion.
  ///
  /// \param inMomentum - The Fermi momentum (kx, ky).
  ///
  /// \return - The partial derivatives (de/dkx, de/dky).
  vector2d<T> derivative(const vector2d<T> &inMomentum) const override {
    T velocityX;
    T velocityY;
    derivative(inMomentum[0], inMomentum[1], velocityX, velocityY);
    return {velocityX, velocityY};
  }

  /// \brief Analytical derivative of the dispersion.
  ///
  /// I.e. the Fermi velocity.
  ///
  /// \param inMomentumX - The Fermi momentum along the x-axis.
  /// \param inMomentumY - The Fermi momentum along the y-axis.
  /// \param outVelocityX - The Fermi velocity along the x-axis.
  /// \param outVelocityY - The Fermi velocity along the y-axis.
  ///
  /// \return - Void.
  void derivative(const T inMomentumX, const T inMomentumY, T &outVelocityX,
                  T &outVelocityY) const override {
    // Hopping anisotropy.
    const T anisotropyPlus = static_cast<T>(1.0) + m_hoppingAnisotropy;
    const T anisotropyMinus = static_cast<T>(1.0) - m_hoppingAnisotropy;

    // Nearest neighbour.
    const T factorNN = static_cast<T>(2.0) * m_hoppingNN;
    const T termNNdX = factorNN * anisotropyPlus * std::sin(inMomentumX);
    const T termNNdY = factorNN * anisotropyMinus * std::sin(inMomentumY);

    // Next-nearest neighbour.
    const T factorNNN = static_cast<T>(4.0) * m_hoppingNNN;
    const T termNNNdX =
        factorNNN * std::sin(inMomentumX) * std::cos(inMomentumY);
    const T termNNNdY =
        factorNNN * std::cos(inMomentumX) * std::sin(inMomentumY);

    // Next-next-nearest neighbour.
    const T factorNNNN = static_cast<T>(4.0) * m_hoppingNNNN;
    const T termNNNNdX = factorNNNN * anisotropyPlus *
                         std::sin(static_cast<T>(2.0) * inMomentumX);
    const T termNNNNdY = factorNNNN * anisotropyMinus *
                         std::sin(static_cast<T>(2.0) * inMomentumY);

    outVelocityX = (termNNdX + termNNNdX + termNNNNdX);
    outVelocityY = (termNNdY + termNNNdY + termNNNNdY);
  }
};
} // namespace conga

#endif // CONGA_DISPERSION_TIGHT_BINDING_H_
