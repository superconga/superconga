# SuperConga API documentation

Welcome to the [API documentation of SuperConga](https://superconga.gitlab.io/superconga/).


### Generating documentation locally

This documentation can also be built locally after cloning the SuperConga repository. To do so, make sure that doxygen and its dependencies are installed, e.g. on Ubuntu 20.04:
```shell
sudo apt install doxygen-latex doxygen-doc doxygen-gui graphviz
```

Then run the following from the root directory of the repository.
```shell
doxygen docs/doxygen.conf
```

### Useful links

<a href="https://gitlab.com/superconga/superconga" target="_blank">SuperConga GitLab repository</a>

<a href="https://superconga.gitlab.io/superconga-doc/" target="_blank">Documentation and user manual</a>
